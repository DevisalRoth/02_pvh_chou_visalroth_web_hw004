import React, { Component } from 'react'
import Swal from 'sweetalert2';
import 'animate.css';
export default class TableComponent extends Component {
    showAlert = (obj) => {

        Swal.fire({

            title: "ID: " + obj.id + "<br />" + "Gmail: " + obj.gmail + "<br />" + "Name: " + obj.name + "<br />" + "Age: " + obj.age,

            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
            }
        })

    }
    render() {
        return (
            <div>

                <div class="w-full">

                    <h1 className="font-bold text-2xl mt-20 bg-black text-white  py-10 ">LIST USER</h1>
                    <div class="">
                        <table class=" text-l  text-gray-500 w-full text-center">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                <tr className="text-lg font-bold " >
                                    <th scope="col" class="px-6 py-3 ">
                                        ID
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        EMAIL
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        USERNAME
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        AGE
                                    </th>
                                    <th scope="col" class="px-16 py-3">
                                        ACTION
                                    </th>

                                </tr>
                            </thead>
                            <tbody>
                                {this.props.data.map((item) => (
                                    <tr className='even:bg-[#c8f0ed] bg-[#a0d7ff]'>
                                        <td class="px-10 py-4 text-black">{item.id}</td>
                                        <td class="px-10 py-4 text-black">{item.gmail}</td>
                                        <td class="px-10 py-4 text-black">{item.name}</td>
                                        <td class="px-10 py-4 text-black">{item.age}</td>
                                        {/* <td class="px-10 py-4">{item.status}</td> */}
                                        <td>
                                            <button onClick={() => this.props.changePending(item)} class={item.status === "pending" ? 'bg-blue-500  text-white font-bold py-2 px-4 rounded ' : 'bg-green-400  text-white font-bold py-2 px-4 rounded '}  >
                                                {item.status}
                                            </button>
                                            <button onClick={() => this.showAlert(item)} class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ml-2">
                                                Show
                                            </button>
                                        </td>
                                    </tr>
                                ))}

                            </tbody>
                        </table>
                    </div>
                </div>

            </div >
        );
    }
}
