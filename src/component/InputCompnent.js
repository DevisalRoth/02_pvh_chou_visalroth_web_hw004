import React, { Component } from 'react'
import "flowbite"
import TableComponent from './TableComponent';
import 'animate.css';
export default class InputCompnent extends Component {
  constructor() {
    super()
    this.state = {
      nextName: "NotExists",
      nextGmail: "NotExists",
      nextAge: "NotExists",
      status: "pending",
      UserName: [
        // Array Object
        { id: 1, name: "Narak", gmail: "Narak@gmail.com", age: 20, status: "pending" },
        { id: 2, name: "Pak", gmail: "Pak@gmail.com", age: 20, status: "pending" },
        { id: 3, name: "Nak", gmail: "Nak@gmail.com", age: 20, status: "pending" },

      ],
      newInformation: "",

    };
  }
  handleChangeName = (event) => {
    this.setState({
      nextName: event.target.value
    })
  }
  handleChangeGmail = (event) => {
    this.setState({
      nextGmail: event.target.value
    })
  }
  handleChangeAge = (event) => {
    this.setState({
      nextAge: event.target.value
    })
  }

  onSubmit = () => {
    const newObj = {
      id: this.state.UserName.length + 1,
      name: this.state.nextName,
      gmail: this.state.nextGmail,
      age: this.state.nextAge,
      status: "pending"
    }
    this.setState({
      UserName: [...this.state.UserName, newObj], nextName: "NotExists", nextGmail: "NotExists", nextAge: "NotExists",
    })
  }

  changePending = (Allobj) => {
    Allobj.status === "pending" ? Allobj.status = "DONE" : Allobj.status = "pending"
    this.setState({ UserName: [...this.state.UserName] })
    console.log(this.state.UserName);
  }




  render() {
    return (
      <div class=" items-center max-w-[1240px] m-auto">
        <div class="flex flex-col justify-center items-center w-full ">
          {/* Form user input */}
          <p class="font-extrabold text-transparent text-8xl bg-clip-text bg-gradient-to-r from-blue-500 to-green-400 mt-12">Please fill Your infromation</p>
          <div class="mb-4 w-96  ">
            <label class="block text-gray-700 text-1xl font-bold mb-2 mt-12" for="username">
              Your Email
            </label>
            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="username"
              type="email"
              placeholder="💌  Enter Your Gmail"
              onChange={this.handleChangeGmail}
            />

            <label class="block text-gray-700 text-1xl font-bold mb-2 mt-4" for="username">
              Username
            </label>
            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="username"
              type="name"
              placeholder="🤖 Enter Your Name"
              onChange={this.handleChangeName}
            />

            <label class="block text-gray-700 text-1xl font-bold mb-2 mt-4" for="username">
              Age
            </label>
            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="username"
              type="Age"
              placeholder="❤️ Enter Your Age"
              onChange={this.handleChangeAge}
            />


          </div>
          {/* Buton Submit */}
          <button onClick={this.onSubmit} class="bg-gradient-to-r from-green-400 to-blue-500 hover:from-pink-500 hover:to-yellow-500text-white font-bold py-2 px-4 rounded ">
            Register
          </button>

        </div>
        {/* Table for display user Name*/}
        <TableComponent data={this.state.UserName} changePending={this.changePending} />
      </div>
    );
  }
}

